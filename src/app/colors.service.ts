import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { environment } from "../environments/environment";

import { Colors } from "./colors";

@Injectable()
export class ColorsService {
  private colorsData = new BehaviorSubject<Colors[]>([]);
  currentColorsData = this.colorsData.asObservable();

  private chosenColor = new BehaviorSubject<string>("black");
  chosenColorVal = this.chosenColor.asObservable();

  constructor(private http: HttpClient) {
    this.getColorsList();
  }

  getColorsList(): Promise<Colors[]> {
    let promise = new Promise<Colors[]>((resolve, reject) => {
      this.http
        .get(environment.apiUrl + "/color/list")
        .subscribe((data: Colors[]) => {
          this.colorsData.next(data);
          resolve(data);
        });
    });
    return promise;
  }

  changeCurrentColor(colorName) {
    this.chosenColor.next(colorName);
  }

  addColor(color): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this.http.post(environment.apiUrl + "/color", color).subscribe(
        (data: any) => {
          this.getColorsList().then(() => {
            resolve(data);
          });
        },
        (error: any) => {
          reject(error);
        }
      );
    });
    return promise;
  }

  removeColor(colorId): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this.http
        .delete(environment.apiUrl + "/color/item/" + colorId)
        .subscribe((data: any) => {
          this.getColorsList().then(() => {
            resolve();
          });
        });
    });
    return promise;
  }
}
