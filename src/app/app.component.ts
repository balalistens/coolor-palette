import { Component, OnInit } from "@angular/core";

import { ColorsService } from "./colors.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  colorsData = [];
  chosenColor = "";
  constructor(private colorsService: ColorsService) {}

  ngOnInit() {
    this.colorsService.currentColorsData.subscribe(colors => {
      this.colorsData = colors;
    });
    this.colorsService.chosenColorVal.subscribe(color => {
      this.chosenColor = color;
    });
  }

  changeColor(colorName) {
    this.colorsService.changeCurrentColor(colorName);
  }
}
