import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { ManageColorsComponent } from "./manage-colors/manage-colors.component";

import { ColorsService } from "./colors.service";

@NgModule({
  declarations: [AppComponent, HomeComponent, ManageColorsComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ColorsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
