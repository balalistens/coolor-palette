import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { ManageColorsComponent } from "./manage-colors/manage-colors.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "manage-colors", component: ManageColorsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
