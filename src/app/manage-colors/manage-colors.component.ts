import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { ColorsService } from "../colors.service";
import { Colors } from "../colors";

@Component({
  selector: "app-manage-colors",
  templateUrl: "./manage-colors.component.html",
  styleUrls: ["./manage-colors.component.scss"]
})
export class ManageColorsComponent implements OnInit {
  colorsData: Array<Colors> = [];
  colorForm: FormGroup;
  chosenColor = "";
  genericMessage = "";

  constructor(private colorsService: ColorsService) {}

  ngOnInit() {
    this.colorForm = new FormGroup({
      name: new FormControl("", Validators.required),
      description: new FormControl("", Validators.required),
      iconClass: new FormControl()
    });

    this.colorsService.currentColorsData.subscribe(colors => {
      this.colorsData = colors;
    });

    this.colorsService.chosenColorVal.subscribe(color => {
      this.chosenColor = color;
    });
  }

  addColor() {
    this.genericMessage = "";
    this.colorForm.controls.name.markAsDirty();
    this.colorForm.controls.description.markAsDirty();
    if (this.colorForm.valid) {
      this.colorsService
        .addColor(this.colorForm.value)
        .then(data => {
          this.genericMessage = data.message;
        })
        .catch(error => {
          if (error.error && error.error.message)
            this.genericMessage = error.error.message;
          else this.genericMessage = error.statusText;
        });
    }
  }

  removeColor(colorId) {
    this.genericMessage = "";
    this.colorForm.controls.name.reset();
    this.colorForm.controls.description.reset();
    this.colorsService.removeColor(colorId);
  }
}
