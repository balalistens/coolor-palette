import { Component, OnInit } from "@angular/core";

import { ColorsService } from "../colors.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  colorsData = [];
  chosenColor = "";
  constructor(private colorsService: ColorsService) {}

  ngOnInit() {
    this.colorsService.currentColorsData.subscribe(colors => {
      this.colorsData = colors.slice(0, 3);
    });

    this.colorsService.chosenColorVal.subscribe(color => {
      this.chosenColor = color;
    });
  }
}
