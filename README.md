# CoolorPalette

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

Live Demo: <http://5870.s.t4vps.eu/coolor-palette/>

##### Color can have these following values as `name`:
- Predefined color names
- Hexadecimal colors
- RGB colors
- RGBA colors
- HSL colors
- HSLA colors

##### The following `icon classes` are supported:

- <http://ionicons.com/>
- <https://fontawesome.com/v4.7.0/icons/>

##### The Spaceship Logo redirects to the `Home` page

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.